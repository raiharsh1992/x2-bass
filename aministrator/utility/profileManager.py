from aministrator.models import adminLoginMaster

def getAdminProfileFromAdminId(adminId):
    adminProfile = adminLoginMaster.objects.values_list("name","number","email").filter(admId=adminId)
    if adminProfile.count()==1:
        adminProfile = adminProfile[0]
        adminProfileData = {
            "name":adminProfile[0],
            "number":adminProfile[1],
            "email":adminProfile[2]
        }
        return adminProfileData
    else:
        return None

class adminLoginClass:

    def __init__(self, adminId, userName, password, name, email, number, isActive):
        self.adminId = adminId
        self.userName = userName
        self.password = password
        self.name = name
        self.email = email
        self.number = number
        self.isActive =isActive

    def isReadyForUpdate(self):
        isValid = False
        if self.name is not None:
            isValid = True
        if self.password is not None:
            isValid = True
        if self.isActive is not None:
            if self.isActive == True or self.isActive==False:
                isValid = True
            else:
                isValid = False
                return isValid
        if self.email is not None:
            if isValidEmail(self.email):
                isValid = True
            else:
                isValid = False
                return isValid
        if self.number is not None:
            if isValidNumber(self.number):
                isValid = True
            else:
                isValid = False
                return isValid
        return isValid

    def isValidToCreate(self):
        if isValidUserName(self.userName):
            if isValidEmail(self.email):
                if isValidNumber(self.number):
                    return True
                else:
                    return False
            else:
                return False
        else:
            return False

    def createNewProfile(self):
        try:
            adminLoginInfo = adminLoginMaster()
            adminLoginInfo.userName = self.userName
            adminLoginInfo.password = self.password
            adminLoginInfo.name = self.name
            adminLoginInfo.email = self.email
            adminLoginInfo.number = self.number
            adminLoginInfo.isActive = self.isActive
            adminLoginInfo.save()
            return True
        except Exception as e:
            print(e)
            return False

    def updateTheProfileInfo(self):
        try:
            adminLoginInfo = adminLoginMaster.objects.get(admId=self.adminId)
            if self.password is not None:
                adminLoginInfo.password = self.password
            if self.name is not None:
                adminLoginInfo.name = self.name
            if self.email is not None:
                adminLoginInfo.email = self.email
            if self.number is not None:
                adminLoginInfo.number = self.number
            if self.isActive is not None:
                adminLoginInfo.isActive = self.isActive
            adminLoginInfo.save()
            return True
        except Exception as e:
            print(e)
            return False

def isValidUserName(userName):
    adminProfile = adminLoginMaster.objects.values_list().filter(userName=userName)
    if adminProfile.count()>0:
        return False
    else:
        return True

def isValidEmail(email):
    adminProfile = adminLoginMaster.objects.values_list().filter(email=email)
    if adminProfile.count()>0:
        return False
    else:
        return True

def isValidNumber(number):
    adminProfile = adminLoginMaster.objects.values_list().filter(number=number)
    if adminProfile.count()>0:
        return False
    else:
        return True

def totalAdminCount():
    adminProfile = adminLoginMaster.objects.values_list().count()
    return adminProfile

def getAllTheUsers(pageSize,pageCount):
    resultContainer = []
    totalCount = totalAdminCount()
    isNextPage = False
    isPreviousPage = False
    if totalCount>0:
        if pageCount > 1:
            isPreviousPage = True
        totalDisplayed = pageSize*pageCount
        if totalDisplayed<totalCount:
            isNextPage = True
        resultContainer = getTheAdminUserList(pageSize, pageCount)
    return resultContainer, totalCount, isNextPage, isPreviousPage

def getTheAdminUserList(pageSize, pageCount):
    adminInfo = adminLoginMaster.objects.values_list("admId","name","number","email","isActive").order_by("-admId")[(pageCount-1)*pageSize:pageCount*pageSize]
    container = []
    for each in adminInfo:
        container.append({
            "adminId":each[0],
            "name":each[1],
            "number":each[2],
            "email":each[3],
            "isActive":each[4]
        })
    return container

def isValidAdminId(adminId):
    if adminId is None:
        return False
    adminInfo = adminLoginMaster.objects.values_list().filter(admId=adminId)
    if adminInfo.count()==1:
        return True
    else:
        return False
