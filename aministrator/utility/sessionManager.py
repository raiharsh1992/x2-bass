from aministrator.models import adminLoginMaster, adminDevice, adminSession
from utility.generateNewKey import generateNewKey
import jwt
from django.conf import settings
from django.utils import timezone

def isValidUserAdminInfo(userName, password):
    adminLoginInfo = adminLoginMaster.objects.values_list().filter(userName=userName, password=password).count()
    if adminLoginInfo==1:
        return True
    else:
        return False

def getAdminIfFromLoginInfo(userName, password):
    adminLoginInfo = adminLoginMaster.objects.values_list("admId","isActive").filter(userName=userName, password=password)
    if adminLoginInfo.count() ==1 :
        if adminLoginInfo[0][1]==True:
            return adminLoginInfo[0][0]
        else:
            return 0
    else:
        return 0

def isNotValidSessionKey(sessionKey):
    adminSesionInfo = adminSession.objects.values_list("isActive").filter(sessionToken=sessionKey)
    if adminSesionInfo.count()>0:
        isActive = False
        for each in adminSesionInfo:
            if each[0]==True:
                isActive = True
                break
        return isActive
    else:
        return False

def getAdminUserSession(adminId, sessionKey, deviceId):
    adminSesionInfo = adminSession.objects.values_list("isActive","sessionId").filter(sessionToken=sessionKey, adminId=adminId, deviceId=deviceId)
    if adminSesionInfo.count()>0:
        sessionId = None
        for each in adminSesionInfo:
            if each[0]==True:
                sessionId = each[1]
                break
        return sessionId
    else:
        return None

def createNewAdminSession(userName, password, deviceId, deviceKey):
    adminId = getAdminIfFromLoginInfo(userName, password)
    if adminId > 0 and deviceId > 0:
        sessionKey = generateNewKey()
        while isNotValidSessionKey(sessionKey):
            sessionKey = generateNewKey()
        try:
            adminSessionInfo = adminSession()
            adminSessionInfo.adminId = adminLoginMaster.objects.get(admId=adminId)
            adminSessionInfo.deviceId = adminDevice.objects.get(deviceId=deviceId)
            adminSessionInfo.sessionToken = sessionKey
            adminSessionInfo.regDate = timezone.now()
            adminSessionInfo.isActive = True
            adminSessionInfo.save()
            sessionId = getAdminUserSession(adminId, sessionKey, deviceId)
            if sessionId is None:
                return None
            jwtObject = {
                "sessionKey":sessionKey,
                "deviceKey":deviceKey,
                "deviceId":deviceId,
                "sessionId":sessionId,
                "adminId":adminId
            }
            encoded_jwt = jwt.encode(jwtObject, settings.SECRET_KEY, algorithm="HS256")
            return encoded_jwt
        except Exception as e:
            print(e)
            return None
    else:
        return None

def validaAdminSession(adminId,deviceId,sessionKey,sessionId):
    adminSesionInfo = adminSession.objects.values_list("isActive").filter(sessionToken=sessionKey, adminId=adminId, deviceId=deviceId, sessionId=sessionId)
    if adminSesionInfo.count()==1:
        if adminSesionInfo[0][0]==True:
            return True
        else:
            return False
    else:
        return False

def manageTheFcm(fcm, sessionId):
    adminSesionInfo = adminSession.objects.values_list("isActive","sessionId").filter(fcm=fcm)
    isValidData = True
    if adminSesionInfo.count()>0:
        for each in adminSesionInfo:
            if each[0] == True:
                if each[1]!=sessionId:
                    if markTheSessionInactive(sessionId):
                        isValidData=True
                    else:
                        isValidData=False
                        break
    return isValidData

def manageTheWebSocket(websocket, sessionId):
    adminSesionInfo = adminSession.objects.values_list("isActive","sessionId").filter(websocket=websocket)
    isValidData = True
    if adminSesionInfo.count()>0:
        for each in adminSesionInfo:
            if each[0] == True:
                if each[1]!=sessionId:
                    if markTheSessionInactive(sessionId):
                        isValidData=True
                    else:
                        isValidData=False
                        break
    return isValidData

def updateSessionRunTimeData(requestValue, sessionId, requestType):
    try:
        adminSessionInfo = adminSession.objects.get(sessionId=sessionId)
        if requestType == 0:
            adminSessionInfo.fcm = requestValue
        elif requestType == 1:
            adminSessionInfo.websocket = requestValue
        elif requestType == 2:
            adminSessionInfo.endDate = timezone.now()
            adminSessionInfo.logoutDate = timezone.now()
            adminSessionInfo.isActive = False
        else:
            return False
        adminSessionInfo.save()
        return True
    except Exception as e:
        print(e)
        return False

def markTheSessionInactive(sessionId):
    try:
        adminSessionInfo = adminSession.objects.get(sessionId=sessionId)
        adminSessionInfo.endDate = timezone.now()
        adminSessionInfo.isActive = False
        adminSessionInfo.save()
        return True
    except Exception as e:
        print(e)
        return False
