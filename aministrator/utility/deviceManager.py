from aministrator.models import adminDevice
from django.utils import timezone

def isNotValidDeviceAdminKey(deviceKey, deviceType, deviceIP):
    adminDeviceInfo = adminDevice.objects.values_list().filter(deviceCode=deviceKey, type=deviceType, ip=deviceIP).count()
    if adminDeviceInfo!=0:
        return True
    else:
        return False

def getDeviceIdAdmin(deviceKey, deviceType, deviceIP):
    adminDeviceInfo = adminDevice.objects.values_list("deviceId").filter(deviceCode=deviceKey, type=deviceType, ip=deviceIP)
    if adminDeviceInfo.count()==1:
        return adminDeviceInfo[0][0]
    else:
        return None

def validateDeviceIdForSessionAdmin(deviceType, deviceKey):
    adminDeviceInfo = adminDevice.objects.values_list("deviceId").filter(deviceCode=deviceKey, type=deviceType)
    if adminDeviceInfo.count()==1:
        return adminDeviceInfo[0][0]
    else:
        return None

def insertNewDeviceAdminInfo(deviceKey, deviceType, deviceIP):
    try:
        adminDeviceInfo = adminDevice()
        adminDeviceInfo.deviceCode = deviceKey
        adminDeviceInfo.type = deviceType
        adminDeviceInfo.ip = deviceIP
        adminDeviceInfo.lastActivity = timezone.now()
        adminDeviceInfo.regDate = timezone.now()
        adminDeviceInfo.save()
        return True
    except Exception as e:
        print(e)
        return False
