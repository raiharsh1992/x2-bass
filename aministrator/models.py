from djongo import models
from django.utils import timezone
# Create your models here.

class adminLoginMaster(models.Model):
    admId = models.AutoField(primary_key=True)
    userName = models.CharField(max_length=64, unique=True, blank=False, null=False)
    password = models.CharField(max_length=64, unique=False, blank=False, null=False)
    name = models.CharField(max_length=255, unique=False, blank=False, null=False)
    number = models.CharField(max_length=15, unique=True, blank=False, null=False)
    email = models.CharField(max_length=255, unique=True, blank=False, null=False)
    isActive = models.BooleanField()

class adminDevice(models.Model):
    deviceId = models.AutoField(primary_key=True)
    deviceCode = models.CharField(max_length=128, unique=False, blank=False, null=False)
    type = models.CharField(max_length=32, unique=False, blank=False, null=False)
    ip = models.CharField(max_length=255, unique=False, blank=False, null=False)
    lastActivity = models.DateTimeField(null=False, blank=False, unique=False)
    regDate = models.DateTimeField(null=False, blank=False, unique=False)

class adminSession(models.Model):
    sessionId = models.AutoField(primary_key=True)
    adminId = models.ForeignKey(adminLoginMaster, on_delete=models.PROTECT)
    deviceId = models.ForeignKey(adminDevice, on_delete=models.PROTECT)
    sessionToken = models.CharField(max_length=128, unique=True, blank=False, null=False)
    regDate = models.DateTimeField(null=False, unique=False, blank=False)
    endDate = models.DateTimeField(null=True, unique=False, blank=False)
    fcm = models.CharField(max_length=255, unique=False, null=True, blank=False)
    websocket = models.CharField(max_length=255, unique=False, null=True, blank=False)
    logoutDate = models.DateTimeField(null=True, unique=False, blank=False)
    isActive = models.BooleanField()

class uiTabName(models.Model):
    tabId = models.AutoField(primary_key=True)
    tabName = models.CharField(max_length=255, unique=True, blank=False, null=False)
    isActive = models.BooleanField(default=False)

class apiList(models.Model):
    apiId = models.AutoField(primary_key=True)
    apiUrl = models.CharField(max_length=255, unique=True, blank=False, null=False)
    requiresAuth = models.BooleanField(default=False)
    isActive = models.BooleanField(default=False)

class admApiController(models.Model):
    accessId = models.AutoField(primary_key=True)
    admId = models.ForeignKey(adminLoginMaster, on_delete=models.PROTECT)
    apiId = models.ForeignKey(apiList, on_delete=models.PROTECT)

class admTabController(models.Model):
    accessId = models.AutoField(primary_key=True)
    admId = models.ForeignKey(adminLoginMaster, on_delete=models.PROTECT)
    apiId = models.ForeignKey(uiTabName, on_delete=models.PROTECT)
