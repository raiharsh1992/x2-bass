# Generated by Django 3.1.4 on 2021-08-23 15:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('aministrator', '0003_auto_20210822_1634'),
    ]

    operations = [
        migrations.AddField(
            model_name='apilist',
            name='requiresAuth',
            field=models.BooleanField(default=False),
        ),
    ]
