from django.urls import path

from aministrator.views import registerNewDevice, validateUserLogin, getAdminProfile, createNewAdminUser, fetchAllUsers, editAdminUserProfiler, editOtherAdminProfile, updateSessionInfo

app_name = "aministrator"

urlpatterns = [
    path('register-new-device', registerNewDevice, name="registerNewDevice"),
    path('admin-login', validateUserLogin, name="validateUserLogin"),
    path('get-admin-profile', getAdminProfile, name="getAdminProfile"),
    path('create-new-admin-user', createNewAdminUser, name="createNewAdminUser"),
    path('fetch-all-users', fetchAllUsers, name="fetchAllUsers"),
    path('edit-user-profile', editAdminUserProfiler, name="editAdminUserProfiler"),
    path('edit-other-admin-profile', editOtherAdminProfile, name="editOtherAdminProfile"),
    path('update-session-info', updateSessionInfo, name="updateSessionInfo"),
]
