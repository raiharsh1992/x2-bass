import datetime
import json
import os
from django.conf import settings
import jwt
import traceback
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response

from utility.checkRequestIp import visitor_ip_address
from utility.generateNewKey import generateNewKey

from aministrator.utility.deviceManager import isNotValidDeviceAdminKey, insertNewDeviceAdminInfo, getDeviceIdAdmin
from aministrator.utility.sessionManager import isValidUserAdminInfo, createNewAdminSession, manageTheFcm, manageTheWebSocket, updateSessionRunTimeData
from aministrator.utility.profileManager import getAdminProfileFromAdminId, adminLoginClass, getAllTheUsers, isValidAdminId

from decorator.userSessionValidation import check_user_admin_permission

# Create your views here.
@api_view(['GET'])
def registerNewDevice(request):
    try:
        deviceType = request.headers.get('User-Agent')
        deviceIP = visitor_ip_address(request)
        deviceKey = generateNewKey()
        while isNotValidDeviceAdminKey(deviceKey, deviceType, deviceIP):
            deviceKey = generateNewKey()
        if insertNewDeviceAdminInfo(deviceKey, deviceType, deviceIP):
            return Response({"message": "Device registered successfully", "deviceKey":deviceKey},
                            status=status.HTTP_200_OK)
        else:
            return Response({"message": "Issue while creating new device token please try again later"},
                            status=status.HTTP_400_BAD_REQUEST)
    except Exception as e:
        traceback.print_exc()
        return Response({"message": "Internal Server Error"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

@api_view(['POST'])
def validateUserLogin(request):
    try:
        deviceKey = request.headers.get('Device-Authorization')
        deviceType = request.headers.get('User-Agent')
        deviceIP = visitor_ip_address(request)
        deviceId = getDeviceIdAdmin(deviceKey, deviceType, deviceIP)
        if deviceId is not None:
            userName = request.data.get("userName", None)
            password = request.data.get("password", None)
            if userName and password is not None:
                if isValidUserAdminInfo(userName, password):
                    jwt = createNewAdminSession(userName, password, deviceId, deviceKey)
                    if jwt is not None:
                        return Response({"message":"Successfully logged in","jwt":jwt},
                                        status=status.HTTP_200_OK)
                    else:
                        return Response({"message": "Something is not right please try again later"},
                                        status=status.HTTP_400_BAD_REQUEST)
                else:
                    return Response({"message": "Invalid user name or password provided"},
                                    status=status.HTTP_400_BAD_REQUEST)
            else:
                return Response({"message": "Username or password cannot be empty"},
                                status=status.HTTP_400_BAD_REQUEST)
        else:
            print(deviceId)
            print(deviceIP)
            return Response({"message": "Invalid device information passed"},
                            status=status.HTTP_400_BAD_REQUEST)
    except Exception as e:
        traceback.print_exc()
        return Response({"message": "Internal Server Error"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


@api_view(['GET'])
@check_user_admin_permission(get_user=True)
def getAdminProfile(request, adminId):
    try:
        adminProfile = getAdminProfileFromAdminId(adminId)
        if adminProfile is None:
            return Response({"message":"There is some issue we will get back to you"},
                            status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({"message":"Profile fetched successfully","adminProfile":adminProfile},
                            status=status.HTTP_200_OK)
    except Exception as e:
        traceback.print_exc()
        return Response({"message": "Internal Server Error"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

@api_view(['POST'])
@check_user_admin_permission(get_user=True)
def createNewAdminUser(request, adminId):
    try:
        userName = request.data.get("userName", None)
        password = request.data.get("password", None)
        name = request.data.get("name", None)
        number = request.data.get("number", None)
        email = request.data.get("email", None)
        if userName and password and name and number and email is not None:
            adminProfileInfo = adminLoginClass(0, userName, password, name, email, number, True)
            if adminProfileInfo.isValidToCreate():
                if adminProfileInfo.createNewProfile():
                    return Response({"message":"New admin profile created successfully"},
                                    status=status.HTTP_200_OK)
                else:
                    return Response({"message":"Issue while creating new profile"},
                                    status=status.HTTP_400_BAD_REQUEST)
            else:
                return Response({"message":"Not valid information passed to create a new profile"},
                                status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({"message":"Some key values are missing"},
                            status=status.HTTP_400_BAD_REQUEST)
    except Exception as e:
        traceback.print_exc()
        return Response({"message": "Internal Server Error"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

@api_view(['POST','GET'])
@check_user_admin_permission(get_user=False)
def fetchAllUsers(request):
    try:
        pageSize = request.data.get("pageSize",25)
        pageCount = request.data.get("pageCount",1)
        if pageSize>0 and pageSize<501:
            if pageCount>0:
                userList, totalCount, isNextPage, isPreviousPage = getAllTheUsers(pageSize,pageCount)
                if userList is None:
                    return Response({"message":"There is some issue we will get back to you"},
                                    status=status.HTTP_400_BAD_REQUEST)
                else:
                    return Response({"message":"User List fetched successfully","userList":userList,"totalCount":totalCount,"isPreviousPage":isPreviousPage,"isNextPage":isNextPage},
                                    status=status.HTTP_200_OK)
            else:
                return Response({"message":"Invalid page count passed"},
                                status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({"message":"Invalid page size passed"},
                            status=status.HTTP_400_BAD_REQUEST)
    except Exception as e:
        traceback.print_exc()
        return Response({"message": "Internal Server Error"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

@api_view(['PUT',])
@check_user_admin_permission(get_user=True)
def editAdminUserProfiler(request, adminId):
    try:
        password = request.data.get("password", None)
        name = request.data.get("name", None)
        number = request.data.get("number", None)
        email = request.data.get("email", None)
        isActive = request.data.get("isActive", None)
        if password or name or number or email or isActive is not None:
            adminProfileInfo = adminLoginClass(adminId, None, password, name, email, number, isActive)
            if adminProfileInfo.isReadyForUpdate():
                if adminProfileInfo.updateTheProfileInfo():
                    return Response({"message":"Profile updated successfully"},
                                    status=status.HTTP_200_OK)
                else:
                    return Response({"message":"Issue while updating profile"},
                                    status=status.HTTP_400_BAD_REQUEST)
            else:
                return Response({"message":"Not valid info passed for editing"},
                                status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({"message":"Nothing to update"},
                            status=status.HTTP_400_BAD_REQUEST)
    except Exception as e:
        traceback.print_exc()
        return Response({"message": "Internal Server Error"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

@api_view(['PUT',])
@check_user_admin_permission(get_user=True)
def editOtherAdminProfile(request, adminId):
    try:
        adminIdEdit = request.data.get("adminId", None)
        if isValidAdminId(adminIdEdit)==False:
            return Response({"message":"Invalid admin id selected for editing"},
                            status=status.HTTP_400_BAD_REQUEST)
        password = request.data.get("password", None)
        name = request.data.get("name", None)
        number = request.data.get("number", None)
        email = request.data.get("email", None)
        isActive = request.data.get("isActive", None)
        if password or name or number or email or isActive is not None:
            adminProfileInfo = adminLoginClass(adminIdEdit, None, password, name, email, number, isActive)
            if adminProfileInfo.isReadyForUpdate():
                if adminProfileInfo.updateTheProfileInfo():
                    return Response({"message":"Profile updated successfully"},
                                    status=status.HTTP_200_OK)
                else:
                    return Response({"message":"Issue while updating profile"},
                                    status=status.HTTP_400_BAD_REQUEST)
            else:
                return Response({"message":"Not valid info passed for editing"},
                                status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({"message":"Nothing to update"},
                            status=status.HTTP_400_BAD_REQUEST)
    except Exception as e:
        traceback.print_exc()
        return Response({"message": "Internal Server Error"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

@api_view(['POST'])
@check_user_admin_permission(get_session=True)
def updateSessionInfo(request, sessionId):
    try:
        requestType = request.data.get("requestType", None)
        if requestType is not None and requestType==0 or requestType==1 or requestType==2:
            #0 == FCM, 1== websocket, 2==Logout
            if requestType==0:
                requestValue = request.data.get("requestValue", None)
                if manageTheFcm(requestValue, sessionId) and requestValue is not None:
                    if updateSessionRunTimeData(requestValue, sessionId, requestType):
                        return Response({"message":"FCM updated successfully"},
                                        status=status.HTTP_200_OK)
                    else:
                        return Response({"message":"Issue while updating content"},
                                        status=status.HTTP_400_BAD_REQUEST)
                else:
                    return Response({"message":"Invalid update content"},
                                    status=status.HTTP_400_BAD_REQUEST)
            elif requestType==1:
                requestValue = request.data.get("requestValue", None)
                if manageTheWebSocket(requestValue, sessionId) and requestValue is not None:
                    if updateSessionRunTimeData(requestValue, sessionId, requestType):
                        return Response({"message":"Websocket updated successfully"},
                                        status=status.HTTP_200_OK)
                    else:
                        return Response({"message":"Issue while updating content"},
                                        status=status.HTTP_400_BAD_REQUEST)
                else:
                    return Response({"message":"Invalid update content"},
                                    status=status.HTTP_400_BAD_REQUEST)
            elif requestType==2:
                if updateSessionRunTimeData(None, sessionId, requestType):
                    return Response({"message":"User logged out successfully"},
                                    status=status.HTTP_200_OK)
                else:
                    return Response({"message":"Issue while logging out user"},
                                    status=status.HTTP_400_BAD_REQUEST)
            else:
                return Response({"message":"Invalid request type"},
                                status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({"message":"Invalid update type"},
                            status=status.HTTP_400_BAD_REQUEST)
    except Exception as e:
        traceback.print_exc()
        return Response({"message": "Internal Server Error"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
