from userCoin.models import refferCodeMaster, transactionMaster
from userProfile.models import userProfileMaster
from django.utils import timezone
import random
import traceback

def isValidRefferalCode(redemptionCode):
    refferInfo = refferCodeMaster.objects.values_list().filter(redemptionCode=redemptionCode)
    if refferInfo.count()==1:
        return True
    else:
        return False

def userHasNoRefferCode(userId):
    refferInfo = refferCodeMaster.objects.values_list().filter(profileId=getUserProfileIdFromuserId(userId))
    if refferInfo.count()==0:
        return True
    else:
        return False

def deleteUserRefferCode(userId):
    profileId = getUserProfileIdFromuserId(userId)
    try:
        refferCodeMaster.objects.filter(profileId=profileId).delete()
        return True
    except Exception as e:
        traceback.print_exc()
        return False

def generateNewRefferCode(userId):
    if userHasNoRefferCode(userId):
        refferCode = ''.join(random.choice('0123456789ABCDEF') for i in range(8))
        while isValidRefferalCode(refferCode):
            refferCode = ''.join(random.choice('0123456789ABCDEF') for i in range(8))
        try:
            profileId = getUserProfileIdFromuserId(userId)
            refferInfo = refferCodeMaster()
            refferInfo.coinCount = 5
            refferInfo.redemptionCode = refferCode
            refferInfo.profileId = userProfileMaster.objects.get(profileId=profileId)
            refferInfo.save()
            return True
        except Exception as e:
            traceback.print_exc()
            return False
    else:
        return False

def getUserProfileIdFromuserId(userId):
    userInfo = userProfileMaster.objects.values_list('profileId').filter(userId=userId)
    if userInfo.count()==1:
        return userInfo[0][0]
    else:
        return 0

def createNewUserTransaction(userId, refferalCode):
    refferInfo = refferCodeMaster.objects.values_list().filter(redemptionCode=refferalCode)
    if refferInfo.count()==1:
        refferInfo = refferInfo[0]
        remmeerProfileId = userProfileMaster.objects.values_list("profileId").filter(userId=userId)
        if remmeerProfileId.count()==1:
            remmeerProfileId = remmeerProfileId[0]
        else:
            return False
        if incrementUserCoinCount(refferInfo[1],refferInfo[3]):
            if markTransaction(refferInfo[1],refferalCode,1,2,refferInfo[3]):
                if incrementUserCoinCount(remmeerProfileId[0],refferInfo[3]):
                    if markTransaction(remmeerProfileId[0],refferalCode,1,1,refferInfo[3]):
                        print('122')
                        return True
                    else:
                        decrementUserCoinCount(refferInfo[1],refferInfo[3])
                        decrementUserCoinCount(remmeerProfileId[0],refferInfo[3])
                        unmarkTransaction(refferInfo[1],refferalCode,1,2,refferInfo[3])
                        return False
                else:
                    decrementUserCoinCount(refferInfo[1],refferInfo[3])
                    unmarkTransaction(refferInfo[1],refferalCode,1,2,refferInfo[1])
                    return False
            else:
                decrementUserCoinCount(refferInfo[1],refferInfo[3])
                return False
        else:
            return False
    else:
        return False

def incrementUserCoinCount(profileId, coinCount):
    try:
        userProfileInfo = userProfileMaster.objects.get(profileId=profileId)
        userProfileInfo.toalCoins = getUserCurrentCoin(profileId)+int(coinCount)
        userProfileInfo.save()
        return True
    except Exception as e:
        print(e)
        return False

def getUserCurrentCoin(profileId):
    userProfileInfo = userProfileMaster.objects.values_list("toalCoins").filter(profileId=profileId)
    if userProfileInfo.count()>0:
        return userProfileInfo[0][0]
    else:
        return 0

def markTransaction(profileId, refferalCode, type, subType, coinCount):
    try:
        transactionInfo = transactionMaster()
        transactionInfo.profileId = userProfileMaster.objects.get(profileId=profileId)
        transactionInfo.type = type
        transactionInfo.subType = subType
        transactionInfo.transactionDateTime = timezone.now()
        transactionInfo.trancationAmount = coinCount
        transactionInfo.redemptionCode = refferalCode
        transactionInfo.save()
        return True
    except Exception as e:
        print(e)
        return False

def unmarkTransaction(profileId, refferalCode, type, subType, coinCount):
    try:
        transactionMaster.objects.filter(profileId=profileId,redemptionCode=refferalCode,type=type,subType=subType,trancationAmount=coinCount).delete()
        return True
    except Exception as e:
        print(e)
        return False

def decrementUserCoinCount(profileId, coinCount):
    try:
        userProfileInfo = userProfileMaster.objects.get(profileId=profileId)
        userProfileInfo.toalCoins = getUserCurrentCoin(profileId)-int(coinCount)
        userProfileInfo.save()
        return True
    except Exception as e:
        print(e)
        return False
