from djongo import models
from django.utils import timezone
# Create your models here.
from userProfile.models import userProfileMaster

class transactionMaster(models.Model):
    transactionId = models.AutoField(primary_key=True)
    profileId = models.ForeignKey(userProfileMaster, on_delete=models.PROTECT)
    ################################
    #   type 1 -> debit
    #   type 2 -> credit
    ################################
    type = models.IntegerField(null=False, blank=False, unique=False)
    ################################
    #   type 1, subtype 1 -> refferal code, used
    #   type 1, subtype 2 -> refferal code, shared
    #   type 1, subtype 3 -> new profile created
    #   type 1, subtype 4 -> Purchased
    #   type 1, subtype 5 -> Game winnings
    #   type 2, subtype 1 -> Game Investment
    #   type 2, subtype 2 -> Redeeemed
    ################################
    subType = models.IntegerField(null=False, blank=False, unique=False)
    transactionDateTime = models.DateTimeField(null=False, default=timezone.now(), unique=False)
    trancationAmount = models.IntegerField(unique=False, null=False, default=0)
    redemptionCode = models.CharField(max_length=255, unique=False, null=False, blank=False)

class refferCodeMaster(models.Model):
    referId = models.AutoField(primary_key=True)
    profileId = models.ForeignKey(userProfileMaster, on_delete=models.PROTECT)
    redemptionCode = models.CharField(unique=True, null=False, blank=False, max_length=255,)
    coinCount = models.IntegerField(unique=False, null=False, default=0)
