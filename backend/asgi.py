"""
ASGI config for backend project.

It exposes the ASGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.1/howto/deployment/asgi/
"""

import os

from django.core.asgi import get_asgi_application

flavour = os.environ.get('FLAVOUR', 'development')
if flavour=="staging":
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'backend.settings.staging')
elif flavour=="development":
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'backend.settings.dev')
elif flavour=="production":
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'backend.settings.production')

application = get_asgi_application()
