"""backend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import include

urlpatterns = [
    # path('admin/', admin.site.urls),
    path('user-profile/', include('userProfile.urls', 'userProfile')),
    path('user-content/', include('userContent.urls', 'userContent')),
    path('user-content/admin/', include('userContent.admin.urls', 'userContent')),
    path('user-chat/admin/', include('userChat.admin.urls', 'userChat')),
    path('user-coin/', include('userCoin.urls', 'userCoin')),
    path('user-chat/', include('userChat.urls', 'userChat')),
    path('administrator/', include('aministrator.urls', 'aministrator')),
]
