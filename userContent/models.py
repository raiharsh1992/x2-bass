from djongo import models
from django.utils import timezone
# Create your models here.

class filterMatser(models.Model):
    tagId = models.AutoField(primary_key=True)
    tagName = models.CharField(max_length=255, blank=False, null=False, unique=False)
    isTag = models.BooleanField()
    isActive = models.BooleanField()

class mediaMaster(models.Model):
    mediaId = models.AutoField(primary_key=True)
    mediaName = models.CharField(max_length=255, blank=False, null=False, unique=False)
    mediaType = models.CharField(max_length=255, blank=False, null=False, unique=False)
    mediaUrl = models.CharField(max_length=255, blank=False, null=False, unique=False)
    createdOn = models.DateTimeField(null=True, unique=False, blank=False)
    thumbnail = models.IntegerField(null=True, blank=False, unique=True)
    isActive = models.BooleanField(default=False)

class contentMaster(models.Model):
    contentId = models.AutoField(primary_key=True)
    contentType = models.IntegerField(blank=False, null=False, unique=False)
    keywods = models.TextField()
    title = models.CharField(max_length=255, blank=False, null=False, unique=False)
    description = models.CharField(max_length=255, blank=False, null=False, unique=False)
    content = models.TextField()
    isLiveContent = models.BooleanField(default=False)
    contentEndedOn = models.DateTimeField(null=True, unique=False, blank=False)
    contentCreatedOn = models.DateTimeField(null=True, unique=False, blank=False)
    isActive = models.BooleanField(default=False)

class tagAssociation(models.Model):
    id = models.AutoField(primary_key=True)
    tagId = models.ForeignKey(filterMatser, on_delete=models.PROTECT, unique=False)
    contentId = models.ForeignKey(contentMaster, on_delete=models.PROTECT, unique=False)

class mediaMapper(models.Model):
    id = models.AutoField(primary_key=True)
    mediaId = models.ForeignKey(mediaMaster, on_delete=models.PROTECT, unique=False)
    contentId = models.ForeignKey(contentMaster, on_delete=models.PROTECT, unique=False)
