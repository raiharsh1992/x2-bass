# Generated by Django 3.1.4 on 2021-08-23 18:03

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('userContent', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='contentmaster',
            name='contentCreatedOn',
            field=models.DateTimeField(null=True),
        ),
        migrations.AddField(
            model_name='contentmaster',
            name='contentEndedOn',
            field=models.DateTimeField(null=True),
        ),
        migrations.AddField(
            model_name='contentmaster',
            name='isLiveContent',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='mediamaster',
            name='createdOn',
            field=models.DateTimeField(null=True),
        ),
    ]
