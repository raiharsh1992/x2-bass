from userContent.models import mediaMaster
from django.utils import timezone

def isValidMediaId(mediaId, mediaType="IMG"):
    mediInfo = mediaMaster.objects.values_list().filter(mediaId=mediaId, mediaType=mediaType)
    if mediaInfo.count()==1:
        return True
    else:
        return False

def mediaUrlIsNotUnique(mediaUrl):
    mediaInfo = mediaMaster.objects.values_list().filter(mediaUrl=mediaUrl)
    if mediaInfo.count()==1:
        return True
    else:
        return False

def createNewMedia(mediaUrl, mediaName, mediaType, thumbnailImage):
    try:
        mediaInfo = mediaMaster()
        mediaInfo.mediaName = mediaName
        mediaInfo.mediaType = mediaType
        mediaInfo.mediaUrl = mediaUrl
        mediaInfo.thumbnail = thumbnailImage
        mediaInfo.createdOn = timezone.now()
        mediaInfo.isActive = True
        mediaInfo.save()
        return True
    except Exception as e:
        print(e)
        return False

def generatePaginatedMediaList(pageCount, pageSize, mediaName, mediaType):
    resultContainer = []
    totalCount = totalMediaCount(mediaName, mediaType)
    isNextPage = False
    isPreviousPage = False
    if totalCount>0:
        if pageCount > 1:
            isPreviousPage = True
        totalDisplayed = pageSize*pageCount
        if totalDisplayed<totalCount:
            isNextPage = True
        resultContainer = getTotalPageData(pageSize, pageCount, mediaName, mediaType)
    return resultContainer, totalCount, isNextPage, isPreviousPage

def totalMediaCount(mediaName, mediaType):
    mediaInfo = mediaMaster.objects.values_list()
    if mediaName is not None:
        mediaInfo = mediaInfo.filter(mediaName__contains=mediaName)
    if mediaType is not None:
        mediaInfo = mediaInfo.filter(mediaType=mediaType)
    return mediaInfo.count()

def getMediaObject(mediaId):
    mediaInfo = mediaMaster.objects.values_list("mediaId","mediaName","mediaType","mediaUrl").filter(mediaId=mediaId)
    if mediaInfo.exist():
        container = {
            "mediaId":mediaInfo[0][0],
            "mediaName":mediaInfo[0][1],
            "mediaType":mediaInfo[0][2],
            "mediaUrl":mediaInfo[0][3],
        }
        return container
    else:
        return None

def getTotalPageData(pageSize, pageCount, mediaName, mediaType):
    mediaInfo = mediaMaster.objects.values_list("mediaId","mediaName","mediaType","mediaUrl","createdOn","thumbnail")
    if mediaName is not None:
        mediaInfo = mediaInfo.filter(mediaName__contains=mediaName)
    if mediaType is not None:
        mediaInfo = mediaInfo.filter(mediaType=mediaType)
    mediaInfo = mediaInfo.order_by("-mediaId")[(pageCount-1)*pageSize:pageCount*pageSize]
    container = []
    for each in mediaInfo:
        thumbnail = None
        if each[5] is not None:
            thumbnail = getMediaObject(each[5])
        container.append({
            "mediaId":each[0],
            "mediaName":each[1],
            "mediaType":each[2],
            "mediaUrl":each[3],
            "createdOn":each[4],
            "thumbnail":thumbnail,
        })
    return container
