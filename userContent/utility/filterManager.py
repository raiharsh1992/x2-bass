from userContent.models import filterMatser

def isValidTagForCreation(tagName, isTag):
    tagInfo = filterMatser.objects.values_list().filter(tagName=tagName, isTag__in=[isTag])
    if tagInfo.count()==0:
        return True
    else:
        return False

def createNewTag(tagName, isTag):
    try:
        tagInfo = filterMatser()
        tagInfo.tagName = tagName
        tagInfo.isTag = isTag
        tagInfo.isActive = True
        tagInfo.save()
        return True
    except Exception as e:
        print(e)
        return False

def totalFilterCount(tagName, isTag):
    tagInfo = filterMatser.objects.values_list().filter(isTag__in=[isTag])
    if tagName is not None:
        tagInfo = tagInfo.filter(tagName__contains = tagName)
    return tagInfo.count()

def getTotalPageData(pageSize, pageCount, tagName, isTag):
    tagInfo = filterMatser.objects.values_list("tagId","tagName","isTag","isActive").filter(isTag__in=[isTag])
    if tagName is not None:
        tagInfo = tagInfo.filter(tagName__contains = tagName)
    tagInfo = tagInfo.order_by("-tagId")[(pageCount-1)*pageSize:pageCount*pageSize]
    container = []
    for each in tagInfo:
        container.append({
            "tagId":each[0],
            "tagName":each[1],
            "isTag":each[2],
            "isActive":each[3]
        })
    return container

def generatePaginatedFilterList(pageCount, pageSize, tagName, isTag):
    resultContainer = []
    totalCount = totalFilterCount(tagName, isTag)
    isNextPage = False
    isPreviousPage = False
    if totalCount>0:
        if pageCount > 1:
            isPreviousPage = True
        totalDisplayed = pageSize*pageCount
        if totalDisplayed<totalCount:
            isNextPage = True
        resultContainer = getTotalPageData(pageSize, pageCount, tagName, isTag)
    return resultContainer, totalCount, isNextPage, isPreviousPage

def isValidTagId(tagId):
    tagInfo = filterMatser.objects.values_list.filter(tagId=tagId).count()
    if tagInfo==1:
        return True
    else:
        return False

def editTagInfo(tagName, tagId, isActive):
    try:
        tagInfo = filterMatser.objects.get(tagId=tagId)
        tagInfo.tagName = tagName
        if isActive is not None:
            tagInfo.isActive = isActive
        tagInfo.save()
        return True
    except Exception as e:
        print(e)
        return False
