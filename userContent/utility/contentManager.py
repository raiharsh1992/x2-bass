from userContent.models import contentMaster

class contentDataClass:
    def __init__(self, contentId, contentType, title, description, content, isLiveContent, keywords, tagList, mediaList, contentEndedOn, contentCreatedOn, isActive):
        self.contentId = contentId
        self.contentType = contentType
        self.title = title
        self.description = description
        self.content = content
        self.isLiveContent = isLiveContent
        self.keywords = keywords
        self.tagList = tagList
        self.mediaList = mediaList
        self.contentEndedOn = contentEndedOn
        self.contentCreatedOn = contentCreatedOn
        self.isActive = isActive

    def isValidForCreation(self):
        if isUniqueContentTitle(self.contentType, self.title):
            if isValidTagAndMediaList(self.tagList, self.mediaList):
                #Validate for BLOG ==1
                #Validate for media post == 2
                #Validate for live post == 3
                #Validate for url post == 4
                if self.contentType == 1 or self.contentType == 2 or self.contentType == 3 or self.contentType == 4:
                    if self.title is not None and self.description is not None and self.content is not None:
                        return True
                    else:
                        return False
                else:
                    return False
            else:
                return False


def isUniqueContentTitle(contentType, title):
    contentInfo = contentMaster.objects.values_list().filter(contentType=contentType, title=title).count()
    if contentInfo == 0:
        return True
    else:
        return False

def isValidTagAndMediaList(tagList, mediaList):
    k
