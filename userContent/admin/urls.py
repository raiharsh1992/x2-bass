from django.urls import path

from userContent.admin.views import generateNewSignedUrl, insertNewMedia, getMediaList, createNewFilter, getFilterList, editFilterInfo

app_name = "userContent"

urlpatterns = [
    path('generate_signed_url', generateNewSignedUrl, name="generateNewSignedUrl"),
    path('insert_new_media', insertNewMedia, name="insertNewMedia"),
    path('get_media_list', getMediaList, name="getMediaList"),
    path('create_new_filter', createNewFilter, name="createNewFilter"),
    path('get_filter_list', getFilterList, name="getFilterList"),
    path('edit_filter', editFilterInfo, name="editFilterInfo"),
]
