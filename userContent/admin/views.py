import datetime
import json
import os
from django.conf import settings
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
import traceback

from utility.generateNewKey import generateNewKey
from utility.s3Adapter import S3Adapter
from decorator.userSessionValidation import check_user_admin_permission

from userContent.utility.mediaHandler import isValidMediaId, mediaUrlIsNotUnique, createNewMedia, generatePaginatedMediaList
from userContent.utility.filterManager import isValidTagForCreation, createNewTag, generatePaginatedFilterList, isValidTagId, editTagInfo

@api_view(['POST',])
@check_user_admin_permission(get_user=False)
def generateNewSignedUrl(request):
    try:
        mediaName = request.data.get("mediaName", None)
        mediaType = request.data.get("mediaType", "IMG")
        mediaExtenstion = request.data.get("mediaExtenstion", None)
        if mediaName is not None:
            if mediaType == "VID" and mediaExtenstion == ".mp4" or mediaExtenstion == ".avi":
                thumbnailImage = request.data.get("thumbnailImage", 0)
                mediaUrl = generateNewKey() + mediaExtenstion
                if thumbnailImage>0 and isValidMediaId(thumbnailImage, "IMG"):
                    s3AdapterInfo = S3Adapter(None, mediaUrl, settings.BUCKETS["media"], "video")
                    while mediaUrlIsNotUnique(mediaUrl):
                        mediaUrl = generateNewKey() + mediaExtenstion
                    signed_url, signed_url_data = s3AdapterInfo.generate_signed_url()
                    return Response({"message":"Signed URL created", "signedUrl":signed_url, "signedUrlData":signed_url_data}, status=status.HTTP_200_OK)
                else:
                    return Response({"message":"Invalid thumbnail selected"}, status=status.HTTP_400_BAD_REQUEST)
            elif mediaType == "GIF" and mediaExtenstion == ".gif":
                mediaUrl = generateNewKey() + mediaExtenstion
                s3AdapterInfo = S3Adapter(None, mediaUrl, settings.BUCKETS["media"], "gif")
                while mediaUrlIsNotUnique(mediaUrl):
                    mediaUrl = generateNewKey() + mediaExtenstion
                signed_url, signed_url_data = s3AdapterInfo.generate_signed_url()
                return Response({"message":"Signed URL created", "signedUrl":signed_url, "signedUrlData":signed_url_data}, status=status.HTTP_200_OK)
            elif mediaType == "IMG" and mediaExtenstion == ".jpeg" or mediaExtenstion == ".jpg" or mediaExtenstion == ".png":
                mediaUrl = generateNewKey() + mediaExtenstion
                s3AdapterInfo = S3Adapter(None, mediaUrl, settings.BUCKETS["media"], "img")
                while mediaUrlIsNotUnique(mediaUrl):
                    mediaUrl = generateNewKey() + mediaExtenstion
                signed_url, signed_url_data = s3AdapterInfo.generate_signed_url()
                return Response({"message":"Signed URL created", "signedUrl":signed_url, "signedUrlData":signed_url_data}, status=status.HTTP_200_OK)
            else:
                return Response({"message":"Invalid media type and media extension pair"}, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({"message":"Media name cannot be blank"}, status=status.HTTP_400_BAD_REQUEST)
    except Exception as e:
        traceback.print_exc()
        return Response({"message": "Internal Server Error"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

@api_view(['POST',])
@check_user_admin_permission(get_user=False)
def insertNewMedia(request):
    try:
        mediaUrl = request.data.get("mediaUrl", None)
        mediaName = request.data.get("mediaName", None)
        mediaType = request.data.get("mediaType", "IMG")
        if mediaName is not None:
            if mediaUrlIsNotUnique(mediaUrl) == False:
                if mediaType == "IMG":
                    s3AdapterInfo = S3Adapter(None, mediaUrl, settings.BUCKETS["media"], "img")
                    if s3AdapterInfo.check_file_exists():
                        if createNewMedia(mediaUrl, mediaName, mediaType, None):
                            return Response({"message":"New Image uploaded successfuly"}, status=status.HTTP_200_OK)
                        else:
                            return Response({"message":"Issue while creating new IMAGE"}, status=status.HTTP_400_BAD_REQUEST)
                    else:
                        return Response({"message":"The file has not been uplodaded yet"}, status=status.HTTP_400_BAD_REQUEST)
                elif mediaType == "GIF":
                    s3AdapterInfo = S3Adapter(None, mediaUrl, settings.BUCKETS["media"], "gif")
                    if s3AdapterInfo.check_file_exists():
                        if createNewMedia(mediaUrl, mediaName, mediaType, None):
                            return Response({"message":"New GIF uploaded successfuly"}, status=status.HTTP_200_OK)
                        else:
                            return Response({"message":"Issue while creating new GIF"}, status=status.HTTP_400_BAD_REQUEST)
                    else:
                        return Response({"message":"The file has not been uplodaded yet"}, status=status.HTTP_400_BAD_REQUEST)
                elif mediaType == "VID":
                    s3AdapterInfo = S3Adapter(None, mediaUrl, settings.BUCKETS["media"], "img")
                    if s3AdapterInfo.check_file_exists():
                        thumbnailImage = request.data.get("thumbnailImage", 0)
                        if isValidMediaId(thumbnailImage, "IMG"):
                            if createNewMedia(mediaUrl, mediaName, mediaType, thumbnailImage):
                                return Response({"message":"New GIF uploaded successfuly"}, status=status.HTTP_200_OK)
                            else:
                                return Response({"message":"Issue while creating new GIF"}, status=status.HTTP_400_BAD_REQUEST)
                        else:
                            return Response({"message":"Invalid thumbnail selected"}, status=status.HTTP_400_BAD_REQUEST)
                    else:
                        return Response({"message":"The file has not been uplodaded yet"}, status=status.HTTP_400_BAD_REQUEST)
                else:
                    return Response({"message":"Invalid media type selected"}, status=status.HTTP_400_BAD_REQUEST)
            else:
                return Response({"message":"File already exist"}, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({"message":"File name cannot be blank"}, status=status.HTTP_400_BAD_REQUEST)
    except Exception as e:
        traceback.print_exc()
        return Response({"message": "Internal Server Error"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

@api_view(['POST','GET'])
@check_user_admin_permission(get_user=False)
def getMediaList(request):
    try:
        mediaName = request.data.get("mediaName", None)
        mediaType = request.data.get("mediaType", None)
        pageCount = request.data.get("pageCount", 1)
        pageSize = request.data.get("pageSize", 25)
        if pageCount>0:
            if pageSize>0 and pageSize<501:
                mediaListInfo, totalCount, isNext, isPrevious = generatePaginatedMediaList(pageCount, pageSize, mediaName, mediaType)
                return Response({"message":"Media List fetched successfuly", "totalCount":totalCount, "mediaListInfo":mediaListInfo, "isPrevious":isPrevious, "isNext":isNext}, status=status.HTTP_200_OK)
            else:
                return Response({"message":"Invalid page size"}, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({"message":"Invalid page count"}, status=status.HTTP_400_BAD_REQUEST)
    except Exception as e:
        traceback.print_exc()
        return Response({"message": "Internal Server Error"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

@api_view(['POST',])
@check_user_admin_permission(get_user=False)
def createNewFilter(request):
    try:
        tagName = request.data.get("tagName", None)
        isTag = request.data.get("isTag", True)
        if tagName is not None:
            if isTag == True or isTag == False:
                if isValidTagForCreation(tagName, isTag):
                    if createNewTag(tagName, isTag):
                        return Response({"message":"New Filter created"}, status=status.HTTP_200_OK)
                    else:
                        return Response({"message":"Issue while creating new filter, please try again later"}, status=status.HTTP_400_BAD_REQUEST)
                else:
                    return Response({"message":"Invalid tag information passed"}, status=status.HTTP_400_BAD_REQUEST)
            else:
                return Response({"message":"Invalid tag type value"}, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({"message":"Tag name cannot be empty"}, status=status.HTTP_400_BAD_REQUEST)
    except Exception as e:
        traceback.print_exc()
        return Response({"message": "Internal Server Error"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

@api_view(['POST','GET'])
@check_user_admin_permission(get_user=False)
def getFilterList(request):
    try:
        tagName = request.data.get("tagName", None)
        isTag = request.data.get("isTag", True)
        pageCount = request.data.get("pageCount", 1)
        pageSize = request.data.get("pageSize", 25)
        if isTag == True or isTag == False:
            if pageCount>0:
                if pageSize>0 and pageSize<501:
                    filterInfo, totalCount, isNext, isPrevious = generatePaginatedFilterList(pageCount, pageSize, tagName, isTag)
                    return Response({"message":"Filter List fetched successfuly", "totalCount":totalCount, "filterInfo":filterInfo, "isPrevious":isPrevious, "isNext":isNext}, status=status.HTTP_200_OK)
                else:
                    return Response({"message":"Invalid page size"}, status=status.HTTP_400_BAD_REQUEST)
            else:
                return Response({"message":"Invalid page count"}, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({"message":"Invalid filter type selected"}, status=status.HTTP_400_BAD_REQUEST)
    except Exception as e:
        traceback.print_exc()
        return Response({"message": "Internal Server Error"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

@api_view(['POST',])
@check_user_admin_permission(get_user=False)
def editFilterInfo(request):
    try:
        tagName = request.data.get("tagName", None)
        isTag = request.data.get("isTag", True)
        isActive = request.data.get("isActive", None)
        tagId = request.data.get("tagId", 0)
        if tagName is not None:
            if isTag == True or isTag == False:
                if isValidTagForCreation(tagName, isTag):
                    if tagId>0 and isValidTagId(tagId):
                        if isActive is None or isActive == True or isActive == False:
                            if editTagInfo(tagName, tagId, isActive):
                                return Response({"message":"New Filter created"}, status=status.HTTP_200_OK)
                            else:
                                return Response({"message":"Issue while creating new filter, please try again later"}, status=status.HTTP_400_BAD_REQUEST)
                        else:
                            return Response({"message":"Invalid is status information"}, status=status.HTTP_400_BAD_REQUEST)
                    else:
                        return Response({"message":"Invalid tag id selected"}, status=status.HTTP_400_BAD_REQUEST)
                else:
                    return Response({"message":"Invalid tag information passed"}, status=status.HTTP_400_BAD_REQUEST)
            else:
                return Response({"message":"Invalid tag type value"}, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({"message":"Tag name cannot be empty"}, status=status.HTTP_400_BAD_REQUEST)
    except Exception as e:
        traceback.print_exc()
        return Response({"message": "Internal Server Error"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

@api_view(['POST'])
@check_user_admin_permission(get_user=False)
def createNewContent(request):
    try:
        contentType = request.data.get("contentType", None)
        title = request.data.get("title", None)
        description = request.data.get("description", None)
        content = request.data.get("content", None)
        isLiveContent = request.data.get("isLiveContent", None)
        keywords = request.data.get("keywords", None)
        tagList = request.data.get("tagList", None)
        mediaList = request.data.get("mediaList", None)
        isActive = request.data.get("isActive", False)
        contetObject = contentDataClass(None, contentType, title, description, content, isLiveContent, keywords, tagList, mediaList, None, timezone.now(), isActive)
        if contentObject.isValidForCreation():
            if contentObject.isContentCreated():
                return Response({"message":"New content created successfuly"}, status=status.HTTP_200_OK)
            else:
                return Response({"message":"Issue while creating new content"}, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({"message":"Not valid information passed to create new content"}, status=status.HTTP_400_BAD_REQUEST)
    except Exception as e:
        traceback.print_exc()
        return Response({"message": "Internal Server Error"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
