import hashlib

import boto3
import botocore
from botocore.config import Config
from botocore.exceptions import ClientError
from django.conf import settings


class S3Adapter:
    def __init__(self, file, file_name, bucket_name, folder_name):
        self.file = file
        self.file_name = file_name
        self.bucket_name = bucket_name
        self.folder_name = folder_name

    def upload_file(self):
        s3 = boto3.resource(service_name='s3', region_name="ap-south-1",
                            aws_access_key_id=settings.AWS["ACCESS_KEY_ID"],
                            aws_secret_access_key=settings.AWS["ACCESS_SECRET_KEY"],
                            config=Config(signature_version='s3v4'))
        result = s3.Bucket(self.bucket_name).put_object(Key=self.folder_name + "/" + self.file_name, Body=self.file,
                                                        ACL='public-read')

        client_md5_checksum = hashlib.md5(self.file).hexdigest()
        server_md5_checksum = result.e_tag.strip('"')
        if client_md5_checksum == server_md5_checksum:
            return True
        else:
            return False

    def delete_file(self):
        s3 = boto3.resource(service_name='s3', region_name="ap-south-1",
                            aws_access_key_id=settings.AWS["ACCESS_KEY_ID"],
                            aws_secret_access_key=settings.AWS["ACCESS_SECRET_KEY"],
                            config=Config(signature_version='s3v4'))
        try:
            result = s3.Object(self.bucket_name, self.folder_name + "/" + self.file_name).delete()

            status_code = result["ResponseMetadata"]["HTTPStatusCode"]
            if 200 <= status_code < 300:
                return True
            else:
                return False
        except Exception as e:
            print(e)
            return False

    def check_file_exists(self):
        s3 = boto3.resource(service_name='s3', region_name="ap-south-1",
                            aws_access_key_id=settings.AWS["ACCESS_KEY_ID"],
                            aws_secret_access_key=settings.AWS["ACCESS_SECRET_KEY"],
                            config=Config(signature_version='s3v4'))
        try:
            s3.Object(self.bucket_name, self.folder_name + "/" + self.file_name).load()
        except botocore.exceptions.ClientError as e:
            if e.response['Error']['Code'] == "404":
                return False
            else:
                return False
        else:
            return True

    def generate_signed_url(self):
        s3 = boto3.client('s3', aws_access_key_id=settings.AWS["ACCESS_KEY_ID"],
                          aws_secret_access_key=settings.AWS["ACCESS_SECRET_KEY"],
                          config=Config(signature_version='s3v4'), region_name="ap-south-1")
        try:
            response = s3.generate_presigned_post(Bucket=self.bucket_name, Key=self.folder_name + "/" + self.file_name,
                                                  Fields={'acl': 'public-read', },
                                                  Conditions=[{"acl": "public-read"}, ],
                                                  ExpiresIn=7890000)
            signed_url = response.get("url")
            signed_url_data = response.get("fields")
            return signed_url, signed_url_data

        except ClientError as e:
            print(e)
            return None
