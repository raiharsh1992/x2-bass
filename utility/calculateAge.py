from django.utils import timezone
def calculateAge(birthDate):
    today = timezone.now().date()
    age = today.year - birthDate.year -((today.month, today.day) < (birthDate.month, birthDate.day))

    return age
