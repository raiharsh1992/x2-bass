import uuid

def generateNewKey():
    return uuid.uuid4().hex
