from django.urls import path

from userProfile.views import registerNewDevice, generateNewOtp, validateTheOtp, createNewProfileUser, checkUniqueValues, checkProfileStatus, viewMyProfile, editProfileUser, updateFcmForUser, logoutUser

app_name = "userProfile"

urlpatterns = [
    path('register-new-device', registerNewDevice, name="registerNewDevice"),
    path('request-for-otp', generateNewOtp, name="generateNewOtp"),
    path('validate-otp', validateTheOtp, name="validateTheOtp"),
    path('create-profile', createNewProfileUser, name="createNewProfileUser"),
    path('check-unique-values', checkUniqueValues, name="checkUniqueValues"),
    path('check-profile-status', checkProfileStatus, name="checkProfileStatus"),
    path('view-profile', viewMyProfile, name="viewMyProfile"),
    path('edit-profile', editProfileUser, name="editProfileUser"),
    path('update-fcm-profile', updateFcmForUser, name="updateFcmForUser"),
    path('logout-profile', logoutUser, name="logoutUser"),
]
