from djongo import models
from django.utils import timezone
# Create your models here.

class userLoginMaster(models.Model):
    userId = models.AutoField(primary_key=True)
    phoneNumber = models.CharField(max_length=64, unique=True, blank=False, null=False)
    isActive = models.BooleanField()

class userProfileMaster(models.Model):
    profileId = models.AutoField(primary_key=True)
    userId = models.ForeignKey(userLoginMaster, on_delete=models.PROTECT)
    userName = models.CharField(max_length=255, unique=False, null=False, blank=False)
    email = models.CharField(max_length=255, unique=False, null=False, blank=False)
    gameName = models.CharField(max_length=255, unique=True, null=False, blank=False)
    # 1 means Male, 2 Female, 3 Others
    gender = models.IntegerField(default=1)
    dateOfBirth = models.DateField(default="1992-11-11")
    startDate = models.DateTimeField(null=False)
    toalCoins = models.IntegerField(unique=False, null=False, default=5)
    currentStatus = models.CharField(max_length=255, unique=False, null=False, blank=False, default="Online")

class userDevice(models.Model):
    deviceId = models.AutoField(primary_key=True)
    deviceCode = models.CharField(max_length=128, unique=False, blank=False, null=False)
    type = models.CharField(max_length=32, unique=False, blank=False, null=False)
    ip = models.CharField(max_length=255, unique=False, blank=False, null=False)
    lastActivity = models.DateTimeField(null=False, blank=False, unique=False)
    regDate = models.DateTimeField(null=False, blank=False, unique=False)

class userSession(models.Model):
    sessionId = models.AutoField(primary_key=True)
    userId = models.ForeignKey(userLoginMaster, on_delete=models.PROTECT)
    deviceId = models.ForeignKey(userDevice, on_delete=models.PROTECT)
    sessionToken = models.CharField(max_length=128, unique=True, blank=False, null=False)
    regDate = models.DateField(null=False, unique=False, blank=False)
    endDate = models.DateField(null=True, unique=False, blank=False)
    fcm = models.CharField(max_length=255, unique=False, null=True, blank=False)
    websocket = models.CharField(max_length=255, unique=False, null=True, blank=False)
    logoutDate = models.DateTimeField(null=True, unique=False, blank=False)
    isActive = models.BooleanField()

class otpManager(models.Model):
    otpId = models.AutoField(primary_key=True)
    deviceId = models.ForeignKey(userDevice, on_delete=models.PROTECT)
    otp = models.CharField(max_length=10, unique=False, blank=False, null=False)
    phoneNumber = models.CharField(max_length=64, unique=False, blank=False, null=False)
    sessionId = models.CharField(max_length=64, unique=True, blank=False, null=False)
    createdOn = models.DateTimeField(default=timezone.now(), null=False, unique=False)
    isUsed = models.BooleanField()
