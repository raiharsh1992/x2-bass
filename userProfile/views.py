import datetime
import json
import os
from django.conf import settings
import jwt
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response

from utility.checkRequestIp import visitor_ip_address
from utility.generateNewKey import generateNewKey
from utility.generateOtp import generateOtp
from utility.calculateAge import calculateAge
from userProfile.utility.deviceManager import isNotValidDeviceUserKey, insertNewDeviceUserInfo, getDeviceIdUser
from userProfile.utility.phoneOtp import isValidPhoneNumberForOtp, sendNewOtp, registerNewOtpInSystem, workWithOtp, markOtpAsUsed
from userProfile.utility.userSession import workWithOtpAndCreateSessionAndManageUser, userProfileExist, updateNewFcm, updateUserSession
from userCoin.utility.manageRefferal import isValidRefferalCode, createNewUserTransaction
from userProfile.utility.profileManagement import uniqueGameName, isUniqueEmailAndValidEmail, createNewProfile, deleteUserProfile, getUserProfileInfo, editProfile

from decorator.userSessionValidation import check_user_user_permission
import traceback

@api_view(['GET'])
def registerNewDevice(request):
    try:
        deviceType = request.headers.get('User-Agent')
        deviceIP = visitor_ip_address(request)
        deviceKey = generateNewKey()
        while isNotValidDeviceUserKey(deviceKey, deviceType, deviceIP):
            deviceKey = generateNewKey()
        if insertNewDeviceUserInfo(deviceKey, deviceType, deviceIP):
            return Response({"message": "Device registered successfully", "deviceKey":deviceKey},
                            status=status.HTTP_200_OK)
        else:
            return Response({"message": "Issue while creating new device token please try again later"},
                            status=status.HTTP_400_BAD_REQUEST)
    except Exception as e:
        traceback.print_exc()
        return Response({"message": "Internal Server Error"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

@api_view(['POST'])
def generateNewOtp(request):
    try:
        deviceKey = request.headers.get('Device-Authorization')
        deviceType = request.headers.get('User-Agent')
        deviceIP = visitor_ip_address(request)
        deviceId = getDeviceIdUser(deviceKey, deviceType, deviceIP)
        if deviceId>0:
            phoneNumber = request.data.get("phoneNumber", None)
            if phoneNumber is not None:
                if isValidPhoneNumberForOtp(phoneNumber):
                    otp = generateOtp(6)
                    sessionId = sendNewOtp(otp, phoneNumber)
                    if registerNewOtpInSystem(otp, phoneNumber, deviceId, sessionId):
                        return Response({"message": "Message sent successfully", "sessionKey":sessionId},
                                        status=status.HTTP_200_OK)
                    else:
                        return Response({"message": "Issue while sending OTP pleae try again later"},
                                        status=status.HTTP_400_BAD_REQUEST)
                else:
                    return Response({"message": "Invalid phone number selected"},
                                    status=status.HTTP_400_BAD_REQUEST)
            else:
                return Response({"message": "Phone number cannot be left empty"},
                                status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({"message": "Invalid device Identification provided"},
                            status=status.HTTP_400_BAD_REQUEST)
    except Exception as e:
        traceback.print_exc()
        return Response({"message": "Internal Server Error"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

@api_view(['POST'])
def validateTheOtp(request):
    try:
        deviceKey = request.headers.get('Device-Authorization')
        deviceType = request.headers.get('User-Agent')
        deviceIP = visitor_ip_address(request)
        deviceId = getDeviceIdUser(deviceKey, deviceType, deviceIP)
        if deviceId>0:
            phoneNumber = request.data.get("phoneNumber", None)
            if phoneNumber is not None:
                otp = request.data.get("otp", None)
                sessionKey = request.data.get("sessionKey", None)
                if otp is not None:
                    if sessionKey is not None:
                        otpId = workWithOtp(otp, sessionKey, phoneNumber, deviceId)
                        if otpId>0:
                            jwt, accountStatus = workWithOtpAndCreateSessionAndManageUser(otpId, phoneNumber, deviceId, deviceKey)
                            if jwt is not None:
                                markOtpAsUsed(otpId)
                                return Response({"message": "Session created successfully", "jwt":jwt, "accountStatus":accountStatus},
                                                status=status.HTTP_200_OK)
                            else:
                                return Response({"message": "Issue while creating session please try again later"},
                                                status=status.HTTP_400_BAD_REQUEST)
                        else:
                            return Response({"message": "Invalid OTP passed"},
                                            status=status.HTTP_400_BAD_REQUEST)
                    else:
                        return Response({"message": "Session key cannot be left empty"},
                                        status=status.HTTP_400_BAD_REQUEST)
                else:
                    return Response({"message": "OTP cannot be left empty"},
                                    status=status.HTTP_400_BAD_REQUEST)
            else:
                return Response({"message": "Phone number cannot be left empty"},
                                status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({"message": "Invalid device Identification provided"},
                            status=status.HTTP_400_BAD_REQUEST)
    except Exception as e:
        traceback.print_exc()
        return Response({"message": "Internal Server Error"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

@api_view(['POST'])
@check_user_user_permission(get_user=True)
def createNewProfileUser(request, userId):
    try:
        if userProfileExist(userId):
            return Response({"message": "Your profile already exist"},
                            status=status.HTTP_400_BAD_REQUEST)
        else:
            userName = request.data.get("userName", None)
            email = request.data.get("email", None)
            gameName = request.data.get("gameName", None)
            gender = int(request.data.get("gender", 0))
            refferalCode = request.data.get("refferalCode", None)
            dob = request.data.get("dob", None)
            proceedFromHere = False
            if refferalCode is not None:
                if isValidRefferalCode(refferalCode):
                    proceedFromHere = True
            else:
                proceedFromHere = True

            if proceedFromHere:
                if uniqueGameName(gameName, userId):
                    if isUniqueEmailAndValidEmail(email, userId):
                        if gender>=1 and gender<=3:
                            try:
                                date_input = datetime.datetime.strptime(dob, "%Y-%m-%d").date()
                                if calculateAge(date_input)>=18:
                                    if createNewProfile(gameName, email, userName, userId, date_input, gender):
                                        if refferalCode is None:
                                            return Response({"message": "Profile created successfully"},
                                                            status=status.HTTP_200_OK)
                                        else:
                                            if createNewUserTransaction(userId, refferalCode):
                                                return Response({"message": "Profile created successfully"},
                                                                status=status.HTTP_200_OK)
                                            else:
                                                deleteUserProfile(userId)
                                                return Response({"message": "Issue while creating your profile please try again later"},
                                                                status=status.HTTP_400_BAD_REQUEST)
                                    else:
                                        return Response({"message": "Issue while creating your profile please try again later"},
                                                        status=status.HTTP_400_BAD_REQUEST)
                                else:
                                    return Response({"message": "You must 18 to access the app"},
                                                    status=status.HTTP_400_BAD_REQUEST)
                            except:
                                traceback.print_exc()
                                return Response({"message": "DOB cannot be blank"},
                                                status=status.HTTP_400_BAD_REQUEST)
                        else:
                            return Response({"message": "Invalid GENDER selected"},
                                            status=status.HTTP_400_BAD_REQUEST)
                    else:
                        return Response({"message": "Invalid Email or Email already taken"},
                                        status=status.HTTP_400_BAD_REQUEST)
                else:
                    return Response({"message": "A profile with same GAME NAME already exist"},
                                    status=status.HTTP_400_BAD_REQUEST)
            else:
                return Response({"message": "Invalid refferal code passed"},
                                status=status.HTTP_400_BAD_REQUEST)
    except Exception as e:
        traceback.print_exc()
        return Response({"message": "Internal Server Error"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

@api_view(['POST'])
@check_user_user_permission(get_user=True)
def checkUniqueValues(request, userId):
    try:
        checkingFor = request.data.get("checkingFor",0)
        checkValue = request.data.get("checkValue", None)
        if checkValue is not None:
            if checkingFor==0:
                if uniqueGameName(checkValue, userId):
                    return Response({"message": "Unique Game name provided"},
                                    status=status.HTTP_200_OK)
                else:
                    return Response({"message": "Uh Oh! Game Name already taken"},
                                    status=status.HTTP_400_BAD_REQUEST)
            elif checkingFor==1:
                if isUniqueEmailAndValidEmail(checkValue, userId):
                    return Response({"message": "Unique Email provided"},
                                    status=status.HTTP_200_OK)
                else:
                    return Response({"message": "Uh Oh! Email already taken"},
                                    status=status.HTTP_400_BAD_REQUEST)
            elif checkingFor==2:
                if isValidRefferalCode(checkValue):
                    return Response({"message": "Valid refferal code provided"},
                                    status=status.HTTP_200_OK)
                else:
                    return Response({"message": "Uh Oh! Invalid refferal code"},
                                    status=status.HTTP_400_BAD_REQUEST)
            else:
                return Response({"message": "Invalid request for validation"},
                                status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({"message": "Empty input passed"},
                            status=status.HTTP_400_BAD_REQUEST)
    except Exception as e:
        traceback.print_exc()
        return Response({"message": "Internal Server Error"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

@api_view(['GET'])
@check_user_user_permission(get_user=True)
def checkProfileStatus(request, userId):
    try:
        if userProfileExist(userId):
            return Response({"message": "Your Profile is complete", "status":True},
                            status=status.HTTP_200_OK)
        else:
            return Response({"message": "Incomplete profile", "status":False},
                            status=status.HTTP_200_OK)
    except Exception as e:
        traceback.print_exc()
        return Response({"message": "Internal Server Error"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

@api_view(['GET'])
@check_user_user_permission(get_user=True)
def viewMyProfile(request, userId):
    try:
        if userProfileExist(userId):
            userProfileInfo = getUserProfileInfo(userId)
            if userProfileInfo is not None:
                return Response({"message": "Your Profile is complete", "userProfileInfo":userProfileInfo},
                            status=status.HTTP_200_OK)
            else:
                return Response({"message": "Something is phishy", "status":False},
                                status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({"message": "Incomplete profile", "status":False},
                            status=status.HTTP_400_BAD_REQUEST)
    except Exception as e:
        traceback.print_exc()
        return Response({"message": "Internal Server Error"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

@api_view(['POST'])
@check_user_user_permission(get_user=True)
def editProfileUser(request, userId):
    try:
        if userProfileExist(userId):
            userName = request.data.get("userName", None)
            email = request.data.get("email", None)
            gameName = request.data.get("gameName", None)
            gender = int(request.data.get("gender", 0))
            dob = request.data.get("dob", None)
            if uniqueGameName(gameName, userId):
                if isUniqueEmailAndValidEmail(email, userId):
                    if gender>=1 and gender<=3:
                        try:
                            date_input = datetime.datetime.strptime(dob, "%Y-%m-%d").date()
                            if calculateAge(date_input)>=18:
                                if editProfile(gameName, email, userName, userId, date_input, gender):
                                    return Response({"message": "Profile edited successfully"},
                                                    status=status.HTTP_200_OK)
                                else:
                                    return Response({"message": "Issue while editing your profile please try again later"},
                                                    status=status.HTTP_400_BAD_REQUEST)
                            else:
                                return Response({"message": "You must 18 to access the app"},
                                                status=status.HTTP_400_BAD_REQUEST)
                        except:
                            traceback.print_exc()
                            return Response({"message": "DOB cannot be blank"},
                                            status=status.HTTP_400_BAD_REQUEST)
                    else:
                        return Response({"message": "Invalid GENDER selected"},
                                        status=status.HTTP_400_BAD_REQUEST)
                else:
                    return Response({"message": "Invalid Email or Email already taken"},
                                    status=status.HTTP_400_BAD_REQUEST)
            else:
                return Response({"message": "A profile with same GAME NAME already exist"},
                                status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({"message": "Your profile does not exist"},
                            status=status.HTTP_400_BAD_REQUEST)
    except Exception as e:
        traceback.print_exc()
        return Response({"message": "Internal Server Error"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

@api_view(['POST'])
@check_user_user_permission(get_user=False)
def updateFcmForUser(request):
    try:
        jwtUse = request.headers.get('Device-JWT')
        jwtObject = jwt.decode(jwtUse, settings.SECRET_KEY, algorithms=["HS256"])
        sessionId = jwtObject["sessionId"]
        fcm = request.data.get("fcm", None)
        if fcm is not None:
            if updateNewFcm(sessionId, fcm):
                return Response({"message": "FCM updated successfully"},
                                status=status.HTTP_200_OK)
            else:
                return Response({"message": "Issue while updating FCM"},
                                status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({"message": "Invalid FCM passed"},
                            status=status.HTTP_400_BAD_REQUEST)
    except Exception as e:
        traceback.print_exc()
        return Response({"message": "Internal Server Error"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

@api_view(['GET'])
@check_user_user_permission(get_user=False)
def logoutUser(request):
    try:
        jwtUse = request.headers.get('Device-JWT')
        jwtObject = jwt.decode(jwtUse, settings.SECRET_KEY, algorithms=["HS256"])
        sessionId = jwtObject["sessionId"]
        if updateUserSession(False, sessionId):
            return Response({"message": "User logged out successfully"},
                            status=status.HTTP_200_OK)
        else:
            return Response({"message": "Issue while logging out user"},
                            status=status.HTTP_400_BAD_REQUEST)
    except Exception as e:
        traceback.print_exc()
        return Response({"message": "Internal Server Error"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
