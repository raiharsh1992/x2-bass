from userProfile.models import otpManager
from django.utils import timezone
from utility.generateNewKey import generateNewKey
from userProfile.models import userDevice
import re

def isValidPhoneNumberForOtp(phoneNumber):
    pattern = "^[0-9]{10}$"
    validPhone = False
    count = 0
    if re.match(pattern,phoneNumber):
        now = timezone.now()
        otpInfo = otpManager.objects.values_list("createdOn","isUsed").filter(phoneNumber=phoneNumber)
        if otpInfo.count()>0:
            for each in otpInfo:
                if each[1]==False:
                    difference = now - each[0]
                    minutes = int(difference.total_seconds() / 60)
                    if minutes>15 and count<3:
                        validPhone = True
                        break
                    count = count+1
                else:
                    validPhone = True
                    break
                if count==4:
                    difference = now - each[0]
                    minutes = difference.seconds() / 60
                    if minutes>15:
                        validPhone = True
                        break

        else:
            validPhone = True
    if count<3:
        validPhone = True
    return validPhone

def sendNewOtp(otp, phoneNumber):
    #Code to send the otp to the phone number and get the session
    return generateNewKey()

def registerNewOtpInSystem(otp, phoneNumber, deviceId, sessionId):
    try:
        otpInfo = otpManager()
        otpInfo.deviceId = userDevice.objects.get(deviceId=deviceId)
        otpInfo.otp = otp
        otpInfo.phoneNumber = phoneNumber
        otpInfo.sessionId = sessionId
        otpInfo.createdOn = timezone.now()
        otpInfo.isUsed = False
        otpInfo.save()
        return True
    except Exception as e:
        print(e)
        return False

def markOtpAsUsed(otpId):
    try:
        otpInfo = otpManager.objects.get(otpId=otpId)
        otpInfo.isUsed = True
        otpInfo.save()
        return True
    except Exception as e:
        print(e)
        return False

def workWithOtp(otp, sessionKey, phoneNumber, deviceId):
    otpInfo = otpManager.objects.values_list("createdOn","isUsed", "otpId").filter(deviceId=deviceId, phoneNumber=phoneNumber, sessionId=sessionKey, otp=otp)
    otpId = 0
    if otpInfo.count()>0:
        for each in otpInfo:
            if each[1]==False:
                difference = timezone.now() - each[0]
                minutes = int(difference.total_seconds() / 60)
                if minutes<15:
                    otpId = each[2]
                    break
        return otpId
    else:
        return 0
