from userProfile.models import userDevice
from django.utils import timezone

def isNotValidDeviceUserKey(deviceKey, deviceType, deviceIP):
    userDeviceInfo = userDevice.objects.values_list().filter(deviceCode=deviceKey, type=deviceType, ip=deviceIP).count()
    if userDeviceInfo!=0:
        return True
    else:
        return False

def getDeviceIdUser(deviceKey, deviceType, deviceIP):
    userDeviceInfo = userDevice.objects.values_list("deviceId").filter(deviceCode=deviceKey, type=deviceType, ip=deviceIP)
    if userDeviceInfo.count()==1:
        return userDeviceInfo[0][0]
    else:
        return 0

def insertNewDeviceUserInfo(deviceKey, deviceType, deviceIP):
    try:
        userDeviceInfo = userDevice()
        userDeviceInfo.deviceCode = deviceKey
        userDeviceInfo.type = deviceType
        userDeviceInfo.ip = deviceIP
        userDeviceInfo.lastActivity = timezone.now()
        userDeviceInfo.regDate = timezone.now()
        userDeviceInfo.save()
        return True
    except Exception as e:
        print(e)
        return False

def validateDeviceIdForSession(deviceType, deviceKey):
    userDeviceInfo = userDevice.objects.values_list("deviceId").filter(deviceCode=deviceKey, type=deviceType)
    if userDeviceInfo.count()==1:
        return userDeviceInfo[0][0]
    else:
        return 0
