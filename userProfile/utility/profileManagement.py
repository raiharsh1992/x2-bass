from userProfile.models import userLoginMaster, userProfileMaster
import re
from userCoin.utility.manageRefferal import generateNewRefferCode, deleteUserRefferCode
import traceback
from django.utils import timezone

regex = r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b'

def uniqueGameName(gameName, userId):
    userProfileInfo = userProfileMaster.objects.values_list("userId").filter(gameName=gameName)
    if userProfileInfo.count()==0:
        return True
    else:
        if userProfileInfo[0][0]==userId:
            return True
        else:
            return False

def isUniqueEmailAndValidEmail(email, userId):
    if(re.fullmatch(regex, email)):
        userProfileInfo = userProfileMaster.objects.values_list("userId").filter(email=email)
        if userProfileInfo.count()==0:
            return True
        else:
            if userProfileInfo[0][0]==userId:
                return True
            else:
                return False
    else:
        return False

def createNewProfile(gameName, email, userName, userId, dateOfBirth, gender):
    try:
        userProfileInfo = userProfileMaster()
        userProfileInfo.userName = userName
        userProfileInfo.email = email
        userProfileInfo.gameName = gameName
        userProfileInfo.gender = gender
        userProfileInfo.dateOfBirth = dateOfBirth
        userProfileInfo.startDate = timezone.now()
        userProfileInfo.userId = userLoginMaster.objects.get(userId=userId)
        userProfileInfo.toalCoins = 5
        userProfileInfo.save()
        if generateNewRefferCode(userId):
            return True
        else:
            deleteUserProfile(userId)
            return False
    except Exception as e:
        traceback.print_exc()
        return False

def editProfile(gameName, email, userName, userId, dateOfBirth, gender):
    profileId = getUserProfileIdFromuserId(userId)
    try:
        userProfileInfo = userProfileMaster.objects.get(profileId=profileId)
        userProfileInfo.userName = userName
        userProfileInfo.email = email
        userProfileInfo.gameName = gameName
        userProfileInfo.gender = gender
        userProfileInfo.dateOfBirth = dateOfBirth
        userProfileInfo.save()
        return True
    except Exception as e:
        traceback.print_exc()
        return False

def deleteUserProfile(userId):
    try:
        if deleteUserRefferCode(userId):
            userProfileMaster.objects.filter(userId=userId).delete()
        return True
    except Exception as e:
        print(e)
        return False

def getUserProfileInfo(userId):
    userProfileInfo = userProfileMaster.objects.values_list().filter(userId=userId)
    if userProfileInfo.count()==1:
        userProfileInfo = userProfileInfo[0]
        userData = {
            'userName':userProfileInfo[2],
            'email':userProfileInfo[3],
            'gameName':userProfileInfo[4],
            'gender':userProfileInfo[5],
            'dateOfBirth':userProfileInfo[6],
            'startDate':userProfileInfo[7],
            'toalCoins':userProfileInfo[8],
        }
        return userData
    else:
        return None

def getUserProfileIdFromuserId(userId):
    userInfo = userProfileMaster.objects.values_list('profileId').filter(userId=userId)
    if userInfo.count()==1:
        return userInfo[0][0]
    else:
        return 0
