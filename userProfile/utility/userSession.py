from utility.generateNewKey import generateNewKey
from userProfile.models import userLoginMaster, userProfileMaster, userDevice, userSession, otpManager
import jwt
from django.conf import settings
from django.utils import timezone

def workWithOtpAndCreateSessionAndManageUser(otpId, phoneNumber, deviceId, deviceKey):
    userId = chekIfUserPresentNotCreate(phoneNumber)
    if userId>0:
        sessionKey =  generateNewKey()
        while isValidSessionKeyForTheuser(sessionKey, deviceId, userId):
            sessionKey = generateNewKey()
        if createNewSessionForTheUser(sessionKey, deviceId, userId):
            sessionId = getSessionIdForUser(sessionKey, deviceId, userId)
            if sessionId>0:
                jwtObject = {
                    "sessionKey":sessionKey,
                    "deviceKey":deviceKey,
                    "deviceId":deviceId,
                    "sessionId":sessionId,
                    "userId":userId
                }
                encoded_jwt = jwt.encode(jwtObject, settings.SECRET_KEY, algorithm="HS256")
                if userProfileExist(userId):
                    return encoded_jwt, True
                else:
                    return encoded_jwt, False
            else:
                return None, None
        else:
            return None, None
    else:
        return None, None

def userProfileExist(userId):
    userProfileInfo = userProfileMaster.objects.values_list().filter(userId=userId)
    if userProfileInfo.count()==1:
        return True
    else:
        return False

def getSessionIdForUser(sessionKey, deviceId, userId):
    userSessionInfo = userSession.objects.values_list("sessionId","isActive").filter(deviceId=deviceId, sessionToken=sessionKey, userId=userId)
    print(userSessionInfo)
    if userSessionInfo.count()>0:
        sessionId = 0
        for each in userSessionInfo:
            if each[1]==True:
                sessionId = each[0]
                break
        return sessionId
    else:
        return 0

def createNewSessionForTheUser(sessionKey, deviceId, userId):
    try:
        userSessionInfo = userSession()
        userSessionInfo.userId = userLoginMaster.objects.get(userId=userId)
        userSessionInfo.deviceId = userDevice.objects.get(deviceId=deviceId)
        userSessionInfo.sessionToken = sessionKey
        userSessionInfo.regDate = timezone.now()
        userSessionInfo.isActive = True
        userSessionInfo.save()
        return True
    except Exception as e:
        print(e)
        return False

def isValidSessionKeyForTheuser(sessionKey, deviceId, userId):
    userSessionInfo = userSession.objects.values_list("isActive").filter(deviceId=deviceId, sessionToken=sessionKey, userId=userId)
    if userSessionInfo.count()>0:
        validSession = True
        for each in userSessionInfo:
            if each[0]==True:
                validSession = False
                break
        return validSession
    else:
        return False

def chekIfUserPresentNotCreate(phoneNumber):
    userLoginInfo = userLoginMaster.objects.values_list("userId","isActive").filter(phoneNumber=phoneNumber)
    if userLoginInfo.count()==1:
        if userLoginInfo[0][1]==True:
            return userLoginInfo[0][0]
        else:
            return 0
    else:
        try:
            userLoginInfoCreate = userLoginMaster()
            userLoginInfoCreate.phoneNumber = phoneNumber
            userLoginInfoCreate.isActive = True
            userLoginInfoCreate.save()
            userId = getUserIfFromPhone(phoneNumber)
            return userId
        except Exception as e:
            print(e)
            return False

def getUserIfFromPhone(phoneNumber):
    userLoginInfo = userLoginMaster.objects.values_list("userId","isActive").filter(phoneNumber=phoneNumber)
    if userLoginInfo.count()==1:
        if userLoginInfo[0][1]==True:
            return userLoginInfo[0][0]
        else:
            return 0
    else:
        return 0

def updateNewFcm(sessionId, fcm):
    try:
        userSessionInfo = userSession.objects.get(sessionId=sessionId)
        userSessionInfo.fcm = fcm
        userSessionInfo.save()
        return True
    except Exception as e:
        print(e)
        return False

def updateUserSession(state, sessionId):
    try:
        userSessionInfo = userSession.objects.get(sessionId=sessionId)
        userSessionInfo.isActive = state
        if state == False:
            userSessionInfo.logoutDate = timezone.now()
            userSessionInfo.endDate = timezone.now()
        userSessionInfo.save()
        return True
    except Exception as e:
        print(e)
        return False

def validateUserSessionInfo(userId,deviceId,sessionToken,sessionId):
    sessionInfo = userSession.objects.values_list("isActive").filter(sessionId=sessionId,userId=userId,deviceId=deviceId,sessionToken=sessionToken)
    if sessionInfo.count()==1:
        if sessionInfo[0][0]==True:
            return True
        else:
            return False
    else:
        return False
