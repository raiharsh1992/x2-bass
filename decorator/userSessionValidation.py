from functools import wraps
import json
from rest_framework import status
from rest_framework.response import Response
import requests
import jwt
from django.conf import settings
from userProfile.utility.deviceManager import validateDeviceIdForSession
from userProfile.utility.userSession import validateUserSessionInfo
from aministrator.utility.sessionManager import validaAdminSession
from aministrator.utility.deviceManager import validateDeviceIdForSessionAdmin

def check_user_user_permission(get_user=False, get_session=False):
    def verify_access(function):
        @wraps(function)
        def wrap(request, *args, **kwargs):
            try:
                deviceKey = request.headers.get('Device-Authorization')
                deviceType = request.headers.get('User-Agent')
                jwtUse = request.headers.get('Device-JWT')
                deviceId = validateDeviceIdForSession(deviceType, deviceKey)
                if deviceId>0:
                    if jwt is not None:
                        jwtObject = jwt.decode(jwtUse, settings.SECRET_KEY, algorithms=["HS256"])
                        if 'sessionKey' in jwtObject and 'deviceKey' in jwtObject and 'deviceId' in jwtObject and 'sessionId' in jwtObject and 'userId' in jwtObject:
                            if jwtObject["deviceKey"]==deviceKey and jwtObject["deviceId"] == deviceId:
                                if validateUserSessionInfo(jwtObject["userId"],jwtObject["deviceId"],jwtObject["sessionKey"],jwtObject["sessionId"]):
                                    if get_session:
                                        return function(request, jwtObject["sessionId"], *args, **kwargs)
                                    if get_user:
                                        return function(request, jwtObject["userId"], *args, **kwargs)
                                    else:
                                        return function(request, *args, **kwargs)
                                else:
                                    return Response({"message": "Your token is invalid"}, status=status.HTTP_401_UNAUTHORIZED)
                            else:
                                return Response({"message": "Invalid token passed"}, status=status.HTTP_401_UNAUTHORIZED)
                        else:
                            return Response({"message": "Uh Oh! somethin is FISHY"}, status=status.HTTP_401_UNAUTHORIZED)
                    else:
                        return Response({"message": "No JWT passed"}, status=status.HTTP_400_BAD_REQUEST)
                else:
                    return Response({"message": "Invalid device info passed"}, status=status.HTTP_401_UNAUTHORIZED)
            except Exception as e:
                print(e)
                return Response({"message": "Your token is invalid please try again later"}, status=status.HTTP_401_UNAUTHORIZED)
        return wrap
    return verify_access

def check_user_admin_permission(get_user=False, get_session=False):
    def verify_access(function):
        @wraps(function)
        def wrap(request, *args, **kwargs):
            try:
                deviceKey = request.headers.get('Device-Authorization')
                deviceType = request.headers.get('User-Agent')
                jwtUse = request.headers.get('Device-JWT')
                deviceId = validateDeviceIdForSessionAdmin(deviceType, deviceKey)
                if deviceId>0:
                    if jwt is not None:
                        jwtObject = jwt.decode(jwtUse, settings.SECRET_KEY, algorithms=["HS256"])
                        if 'sessionKey' in jwtObject and 'deviceKey' in jwtObject and 'deviceId' in jwtObject and 'sessionId' in jwtObject and 'adminId' in jwtObject:
                            if jwtObject["deviceKey"]==deviceKey and jwtObject["deviceId"] == deviceId:
                                # currentUrl = request.META["PATH_INFO"]
                                # Infuture use this value to restrict api levele access
                                if validaAdminSession(jwtObject["adminId"],jwtObject["deviceId"],jwtObject["sessionKey"],jwtObject["sessionId"]):
                                    if get_session:
                                        return function(request, jwtObject["sessionId"], *args, **kwargs)
                                    if get_user:
                                        return function(request, jwtObject["adminId"], *args, **kwargs)
                                    else:
                                        return function(request, *args, **kwargs)
                                else:
                                    return Response({"message": "Your token is invalid"}, status=status.HTTP_401_UNAUTHORIZED)
                            else:
                                return Response({"message": "Invalid token passed"}, status=status.HTTP_401_UNAUTHORIZED)
                        else:
                            return Response({"message": "Uh Oh! somethin is FISHY"}, status=status.HTTP_401_UNAUTHORIZED)
                    else:
                        return Response({"message": "No JWT passed"}, status=status.HTTP_400_BAD_REQUEST)
                else:
                    return Response({"message": "Invalid device info passed"}, status=status.HTTP_401_UNAUTHORIZED)
            except Exception as e:
                print(e)
                return Response({"message": "Your token is invalid please try again later"}, status=status.HTTP_401_UNAUTHORIZED)
        return wrap
    return verify_access
