import datetime
import json
import logging
import os
from django.conf import settings
from rest_framework.decorators import api_view
from rest_framework.response import Response

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(levelname)s:%(asctime)s:%(message)s')
log_file_path = os.path.join(os.path.dirname(BASE_DIR), "log/account/AppUserAccount.log")
file_handler = logging.FileHandler(log_file_path)
file_handler.setFormatter(formatter)
stream_handler = logging.StreamHandler()
stream_handler.setFormatter(formatter)
logger.addHandler(file_handler)
logger.addHandler(stream_handler)

@api_view(['POST', 'GET'])
def get_me_tab_details(request):
    try:
        token = request.headers.get('Authorization')
        token = Token.objects.get(key=token[6:])
        user = token.user
        date_input = request.GET.get("date_input", None)
        try:
            date_input = datetime.datetime.strptime(date_input, "%Y-%m-%d").date()
        except:
            return Response({"message": "Date not given or not provided in the required format"},
                            status=status.HTTP_400_BAD_REQUEST)

        start_time = DateFormatHelper.get_utc_start_time(date_input)
        end_time = DateFormatHelper.get_utc_end_time(date_input)
        if start_time and end_time:
            if start_time < end_time:
                meTabInformationHelper = MeTabInformationHelper(user, start_time, end_time)
                response = {}
                response["profile_picture_name"] = meTabInformationHelper.get_profile_picture_image_name()
                response["onboarding_date"] = meTabInformationHelper.get_user_on_boarding_date()
                response["goal_calorie_in_value"] = meTabInformationHelper.get_goal_calorie_in_value()
                response["image"] = meTabInformationHelper.get_transformation_image_name()
                response["logged_calorie_in_value"] = meTabInformationHelper.get_logged_calorie_in_value()
                response["goal_calorie_out_value"] = meTabInformationHelper.get_goal_calorie_out_value()
                response["logged_calorie_out_value"] = meTabInformationHelper.get_logged_calorie_out_value()
                response["user_scale_data"] = meTabInformationHelper.get_user_scale_details()
                response["user_body_measurement_data"] = meTabInformationHelper.get_body_measurement_details()
                return Response(response, status=status.HTTP_200_OK)
            else:
                return Response({"message": "Start Time Cannot be greater than End Time"},
                                status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({"message": "Invalid Date Given"}, status=status.HTTP_400_BAD_REQUEST)
    except Exception as e:
        logger.exception(e)
        return Response({"message": "Internal Server Error"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
