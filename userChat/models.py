from djongo import models
from django.utils import timezone
# Create your models here.
from userProfile.models import userProfileMaster
from aministrator.models import adminLoginMaster

class groupMaster(models.Model):
    gId = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255, unique=False, blank=False, null=False)
    description = models.CharField(max_length=255, unique=False, blank=False, null=False)
    avatarUrl = models.CharField(max_length=255, unique=False, blank=False, null=False)
    type = models.IntegerField()
    createdOn = models.DateTimeField(default=timezone.now())
    createdByUserId = models.ForeignKey(userProfileMaster, on_delete=models.PROTECT, null=True)
    createdByAdminId = models.ForeignKey(adminLoginMaster, on_delete=models.PROTECT, null=True)

class participationMaster(models.Model):
    pId = models.AutoField(primary_key=True)
    gId = models.ForeignKey(groupMaster, on_delete=models.PROTECT)
    userId = models.ForeignKey(userProfileMaster, on_delete=models.PROTECT, null=True)
    adminId = models.ForeignKey(adminLoginMaster, on_delete=models.PROTECT, null=True)
    isAccepted = models.BooleanField()
    acceptionDate = models.DateTimeField(null=True, blank=False, unique=False)
    addedOn = models.DateTimeField(null=True, blank=False, unique=False)
    createdByUserId = models.IntegerField(null=True)
    createdByAdminId = models.IntegerField(null=True)

class messageMaster(models.Model):
    mId = models.AutoField(primary_key=True)
    gId = models.ForeignKey(groupMaster, on_delete=models.PROTECT)
    sentByUserId = models.ForeignKey(userProfileMaster, on_delete=models.PROTECT, null=True)
    sentByAdminId = models.ForeignKey(adminLoginMaster, on_delete=models.PROTECT, null=True)
    sentOnDateTime = models.DateTimeField(default=timezone.now())
    message = models.TextField()
    isDeleted = models.BooleanField(default=False)

class recieveId(models.Model):
    rId = models.AutoField(primary_key=True)
    mId = models.ForeignKey(messageMaster, on_delete=models.PROTECT)
    userId = models.ForeignKey(userProfileMaster, on_delete=models.PROTECT, null=True)
    adminId = models.ForeignKey(adminLoginMaster, on_delete=models.PROTECT, null=True)
    seenOn = models.DateTimeField(null=True, unique=False)
    recievedOn = models.DateTimeField(null=True, unique=False)
