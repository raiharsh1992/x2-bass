from django.apps import AppConfig


class UserchatConfig(AppConfig):
    name = 'userChat'
